# Demonstrates how to do simple monitoring

## Features

- each monitoring file, should be callable
- monitoring checks should be under src/monitoring
- no "glue" code for task/ needed
- Spark context can be reused