import importlib
import glob


for module in glob.glob('src/monitoring/**/*.py', recursive=True):
    if module.endswith('__init__.py'):
        continue
    importlib.import_module(
        module.replace('src/', '').replace('/', '.').replace('.py', ''))

del module
del glob
del importlib
